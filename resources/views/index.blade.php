@extends('layouts/app')
@section('title', 'Tous les articles')
@section('content')
<div class="content">
    <h1>Tous les articles</h1>

    <p>
        <!-- Lien pour créer un nouvel article : "create" -->
        <a href="{{ route('posts.create') }}" title="Créer un article">Créer un nouveau post</a>
    </p>

    <!-- Le tableau pour lister les articles/posts -->
    <table border="1" class="tab">
        <thead>
            <tr>
                <th>Titre</th>
                <th colspan="2">Opérations</th>
            </tr>
        </thead>
        <tbody>
            <!-- On parcourt la collection de Post -->
            @if ($posts)
                @foreach ($posts as $post)
                    <tr>
                        <td>
                            <!-- Lien pour afficher un Post : "show" -->
                            <a href="{{ route('posts.show', ['id' => $post['id']]) }}" title="Lire l'article">{{ $post['title'] }}</a>
                        </td>
                        <td>
                            <!-- Lien pour modifier un Post : "posts.edit" -->
                            <a href="{{ route('posts.edit', ['id' => $post['id']]) }}" title="Modifier l'article">Modifier</a>
                        </td>
                        <td>
                            <a href="{{ route('posts.destroy', $post['id']) }}">Supprimer</a>
                        </td>
                    </tr>
                @endforeach
                @else
                <span>Aucun post actuellement.</span>
            @endif
        </tbody>
    </table>


</div>
@endsection
