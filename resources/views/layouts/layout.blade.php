<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Le titre de la page -->
    <title>@yield('title')</title>
</head>

<body>

    <nav class="navbar">
        <img src="{{url('storage/images/logo.png')}}" alt="Logo du blog" class="logo"/>
        <ul class="navlink">
            <li><a href="{{ route('index') }}">Accueil</a></li>
        </ul>
    </nav>
    <!-- Le contenu -->
    @yield('content')
    @yield('create')

</body>

</html>
