@extends("layouts.layout")
@section("title", "Modifier un poste")
@section("content")

	<h1>Modifier le poste</h1>

	<!-- Le formulaire est géré par la route "posts.store" -->
	<form method="POST" action="{{ route('posts.update') }}" enctype="multipart/form-data" >

		<!-- Le token CSRF -->
		@csrf

		<p>
            <input type="hidden" name="id" value="{{ $post['id'] }}">
			<label for="title" >Titre</label><br/>
			<input type="text" name="title" value="{{ $post['title'] }}"  id="title" placeholder="Le titre du post" >

			<!-- Le message d'erreur pour "title" -->
			@error("title")
			<div>{{ $message }}</div>
			@enderror
		</p>
		<p>
			<label for="content" >Contenu</label><br/>
			<textarea name="content" id="content" lang="fr" rows="10" cols="50" placeholder="Le contenu du post" >{{ $post['content'] }}</textarea>

			<!-- Le message d'erreur pour "content" -->
			@error("content")
			<div>{{ $message }}</div>
			@enderror
		</p>

		<input type="submit" name="valider" value="Valider" >

	</form>

    <p><a href="{{ route('index') }}" title="Retourner aux articles" >Retourner aux posts</a></p>

@endsection
