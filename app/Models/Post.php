<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts';

    public int $id;
    public string $title;
    public string $picture;
    public string $content;

    protected $fillable = [
        'title',
        'picture',
        'content'
      ];


}
