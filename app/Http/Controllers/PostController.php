<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = [
            'title' => 'bail|required|string|max:255',
            "picture" => 'bail|required|image|max:1024',
            "content" => 'bail|required'
        ];

        $this->validate($request, $rules);

        $post = new Post();
        $post->title = $request->title;
        $post->picture = $request->picture;
        $post->content = $request->content;

        $chemin_image = $request->picture->store("posts");

        Post::create([
            'title' => $request->title,
            'picture' => $chemin_image,
            'content' => $request->content
        ]);
        // 4. On retourne vers tous les posts : route("posts.index")
        return redirect(route("index"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('edit', compact([
            'post', 'id'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'title' => 'bail|required|string|max:255',
            "content" => 'bail|required'
        ];

        $this->validate($request, $rules);

        Post::where('id', $request->id)->update([
            'title' => $request->title,
            'content' => $request->content
        ]);
        // 4. On retourne vers tous les posts : route("posts.index")
        return redirect(route("index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        Post::where('id', $id)->firstOrFail()->delete();
        return redirect(route("index"));
    }
}
